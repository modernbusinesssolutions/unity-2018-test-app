using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static Pushnami;

public class PushnamiInit : MonoBehaviour
{
    public string FCMApiKey;
    public string FCMAppID;
    public string FCMSenderID;
    public string PushnamiApiKey;
    public string PushnamiAppID;
    public string PushnamiWebsiteID;

    void Start()
    {
        #if UNITY_ANDROID && !UNITY_EDITOR
            Pushnami.InitAndroid(FCMApiKey, FCMAppID, FCMSenderID, PushnamiApiKey, PushnamiAppID, PushnamiWebsiteID);
        #else
            Debug.Log("Pushnami code currently running outside of Android environment. Not initializing.");
        #endif
    }
}
