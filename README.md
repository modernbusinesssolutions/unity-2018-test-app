# Unity 2018 Test App

An empty Unity 2018 test application with the Pushnami plugin included.

## Installation

1. Open project in Unity Editor
2. Navigate to **SampleScene** -> **Main Camera** -> **New Sprite**
3. Replace **CHANGE ME** with your FCM and Pushnami credentials in the attached **PushnamiInit** script
4. Re-run Android dependency resolver (all .jars should be included in Assets/Plugins/Android but run it again just to be sure)
5. Build the application
6. Upon running the application, you should see your PSID and notice of successful initialization in Logcat:

```
2020-08-14 11:30:07.282 7448-7448/? I/Pushnami: Successfully subscribed with ID: 5f36bc0e3244a20012ac05f7
```

7. Send yourself a test push. 

During the test process, it is important not to kill the app from the app switcher in Android. You may close the app and go back to the home screen, but do not swipe up on the app to kill it, as this will prevent Firebase messages from being received. This is a known behavior on debug apps and should not happen with signed release APKs.
